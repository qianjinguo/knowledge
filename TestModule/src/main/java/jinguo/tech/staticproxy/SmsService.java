package jinguo.tech.staticproxy;

/**
 * @author jinguo
 */
public interface SmsService {
    String sendMessage(String message);
}
