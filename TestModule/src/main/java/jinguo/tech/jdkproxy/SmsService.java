package jinguo.tech.jdkproxy;

/**
 * @author jinguo
 */
public interface SmsService {
    String sendMessage(String message);
}
